package bbrenderer

import (
    "fmt"
    "bytes"

    "github.com/russross/blackfriday"
)

// A heading style
type FontStyle struct {
    // Size of the font, or 0 if the default font size should be used
    FontSize    int

    // Font style
    Bold        bool
    Italic      bool
    Underline   bool

    // Color style
    Color       string
}

func (fs FontStyle) openTag(out *bytes.Buffer) {
    if (fs.FontSize > 0) {
        fmt.Fprintf(out, "[size=%d]", fs.FontSize)
    }
    if (fs.Bold) {
        out.WriteString("[b]")
    }
    if (fs.Italic) {
        out.WriteString("[i]")
    }
    if (fs.Underline) {
        out.WriteString("[u]")
    }
    if (fs.Color != "") {
        fmt.Fprintf(out, "[color=%s]", fs.Color)
    }
}

func (fs FontStyle) closeTag(out *bytes.Buffer) {
    if (fs.FontSize > 0) {
        out.WriteString("[/size]")
    }
    if (fs.Bold) {
        out.WriteString("[/b]")
    }
    if (fs.Italic) {
        out.WriteString("[/i]")
    }
    if (fs.Underline) {
        out.WriteString("[/u]")
    }
    if (fs.Color != "") {
        out.WriteString("[/color]")
    }
}


// A PhpBB renderer.  
type PhpBBRenderer struct {
    // Heading font sizes.  If not defined, the heading will not be resized
    HeadingFontStyles    map[int]FontStyle
}

func New() *PhpBBRenderer {
    return &PhpBBRenderer{
        HeadingFontStyles: map[int]FontStyle {
            1: FontStyle{ FontSize: 5, Bold: true },
            2: FontStyle{ FontSize: 4, Bold: true },
            3: FontStyle{ Bold: true },
            4: FontStyle{ Bold: true },
            5: FontStyle{ Bold: true },
        },
    }
}

func (bb *PhpBBRenderer) BlockCode(out *bytes.Buffer, text []byte, lang string) {
    out.WriteString("\n")
    out.WriteString("[code]\n")
    out.Write(text)
    out.WriteString("[/code]\n\n")
}

func (bb *PhpBBRenderer) BlockQuote(out *bytes.Buffer, text []byte) {

}

func (bb *PhpBBRenderer) BlockHtml(out *bytes.Buffer, text []byte) {

}

func (bb *PhpBBRenderer) Header(out *bytes.Buffer, text func() bool, level int, id string) {
    fontStyle, hasFontStyle := bb.HeadingFontStyles[level]

    out.WriteString("\n")
    if (hasFontStyle) {
        fontStyle.openTag(out)
        text()
        fontStyle.closeTag(out)
    } else {
        text()
    }
    out.WriteString("\n\n")
}

func (bb *PhpBBRenderer) HRule(out *bytes.Buffer) {
    out.WriteString("[hr]")
}

func (bb *PhpBBRenderer) List(out *bytes.Buffer, text func() bool, flags int) {
    isOrdered := (flags & blackfriday.LIST_TYPE_ORDERED) != 0

    out.WriteString("\n")
    if isOrdered {
        out.WriteString("[ol]\n")
        text()
        out.WriteString("[/ol]\n")
    } else {
        out.WriteString("[ul]\n")
        text()
        out.WriteString("[/ul]\n")
    }

}

func (bb *PhpBBRenderer) ListItem(out *bytes.Buffer, text []byte, flags int) {
    out.WriteString("[li]")
    out.Write(text)
    out.WriteString("\n")
}

func (bb *PhpBBRenderer) Paragraph(out *bytes.Buffer, text func() bool) {
    text()
    out.WriteString("\n\n")
}

func (bb *PhpBBRenderer) Table(out *bytes.Buffer, header []byte, body []byte, columnData []int) {
    out.WriteString("\n[table]\n")
    out.Write(header)
    out.Write(body)
    out.WriteString("[/table]")
}

func (bb *PhpBBRenderer) TableRow(out *bytes.Buffer, text []byte) {
    out.WriteString("[tr]")
    out.Write(text)
    out.WriteString("[/tr]\n")
}

func (bb *PhpBBRenderer) TableHeaderCell(out *bytes.Buffer, text []byte, flags int) {
    out.WriteString("[th]")
    out.Write(text)
    out.WriteString("[/th]")
}

func (bb *PhpBBRenderer) TableCell(out *bytes.Buffer, text []byte, flags int) {
    out.WriteString("[td]")
    out.Write(text)
    out.WriteString("[/td]")
}

func (bb *PhpBBRenderer) Footnotes(out *bytes.Buffer, text func() bool) {

}

func (bb *PhpBBRenderer) FootnoteItem(out *bytes.Buffer, name, text []byte, flags int) {

}

func (bb *PhpBBRenderer) TitleBlock(out *bytes.Buffer, text []byte) {
    out.Write(text)
}

// Span-level callbacks
func (bb *PhpBBRenderer) AutoLink(out *bytes.Buffer, link []byte, kind int) {
    out.WriteString("[url=")
    out.Write(link)
    out.WriteString("]")
    out.Write(link)
    out.WriteString("[/url]")
}

func (bb *PhpBBRenderer) CodeSpan(out *bytes.Buffer, text []byte) {
    // TEMP
    out.WriteString("\"")
    out.Write(text)
    out.WriteString("\"")
}

func (bb *PhpBBRenderer) DoubleEmphasis(out *bytes.Buffer, text []byte) {
    out.WriteString("[b]")
    out.Write(text)
    out.WriteString("[/b]")
}

func (bb *PhpBBRenderer) Emphasis(out *bytes.Buffer, text []byte) {
    out.WriteString("[i]")
    out.Write(text)
    out.WriteString("[/i]")
}

func (bb *PhpBBRenderer) Image(out *bytes.Buffer, link []byte, title []byte, alt []byte) {
}

func (bb *PhpBBRenderer) LineBreak(out *bytes.Buffer) {
    out.WriteString("\n\n")
}

func (bb *PhpBBRenderer) Link(out *bytes.Buffer, link []byte, title []byte, content []byte) {
    out.WriteString("[url=")
    out.Write(link)
    out.WriteString("]")
    out.Write(content)
    out.WriteString("[/url]")
}

func (bb *PhpBBRenderer) RawHtmlTag(out *bytes.Buffer, tag []byte) {
    out.Write(tag)
}

func (bb *PhpBBRenderer) TripleEmphasis(out *bytes.Buffer, text []byte) {
    out.WriteString("[b][i]")
    out.Write(text)
    out.WriteString("[/i][/b]")
}

func (bb *PhpBBRenderer) StrikeThrough(out *bytes.Buffer, text []byte) {

}

func (bb *PhpBBRenderer) FootnoteRef(out *bytes.Buffer, ref []byte, id int) {

}


// Low-level callbacks
func (bb *PhpBBRenderer) Entity(out *bytes.Buffer, entity []byte) {
    out.Write(entity)
}

func (bb *PhpBBRenderer) NormalText(out *bytes.Buffer, text []byte) {
    out.Write(text)
}


// Header and footer
func (bb *PhpBBRenderer) DocumentHeader(out *bytes.Buffer) {

}

func (bb *PhpBBRenderer) DocumentFooter(out *bytes.Buffer) {

}


func (bb *PhpBBRenderer) GetFlags() int {
    return 0
}
