package main

import (
    "fmt"
    "os"
    "bytes"
    "github.com/russross/blackfriday"

    "bitbucket.org/lmika/mdtobb/bbrenderer"
)


func main() {
    inBuffer := new(bytes.Buffer)
    inBuffer.ReadFrom(os.Stdin)

    exts := blackfriday.EXTENSION_TABLES | blackfriday.EXTENSION_FENCED_CODE | blackfriday.EXTENSION_AUTOLINK
    
    mdRes := blackfriday.Markdown(inBuffer.Bytes(), bbrenderer.New(), exts)
    fmt.Print(string(mdRes))
}