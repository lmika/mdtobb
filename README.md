Markdown To PhpBB
=================

An unbelieviably simple CLI app which converts Markdown to phpBB markup.
Written to avoid the need to work with the abomination that is phpBB.

Written as a renderer for [Black Friday](https://github.com/russross/blackfriday) but
usable as a standalone app as well.  **NOTE**: Not all methods are implemented yet.