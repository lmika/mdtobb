import sublime, sublime_plugin
import subprocess

class MdToBbCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        region = sublime.Region(0, self.view.size())
        mdText = self.view.substr(region)

        bbText = self._convert_md_to_bb(mdText)
        bbText.strip()

        # Display the result in an output window
        viewWindow = self.view.window()

        outputView = viewWindow.create_output_panel("md_to_bb")
        viewWindow.run_command("show_panel", {"panel": "output.md_to_bb"})
        outputView.insert(edit, 0, bbText)

        viewWindow.focus_view(outputView)
        outputView.run_command("select_all")

    def _convert_md_to_bb(self, in_md):
        proc = subprocess.Popen(["mdtobb"], \
            stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)

        try:
            outs, errs = proc.communicate(input=in_md, timeout=5000)
            return outs
        except subprocess.TimeoutExpired:
            proc.kill()
            outs, errs = proc.communicate()
            return "Error from mdtobb"
