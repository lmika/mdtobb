Markdown To BB Sublime Text 3 Plugin
====================================

A Sublime Text 3 plugin which will produce BB from a Markdown file when the user presses Ctrl+Shift+Equals.

To Install
----------

    1. Install `mdtobb` into the path.
    2. Create a symbolic link from the Sublime Text 3 `Packages` directory to `_sublime-3-package/MarkdownToBBPackage`

Usage
-----

The following key-strokes are provided:

    - `Ctrl+Shift+Equals`: Generate BB using the Markdown in the current view and display it in a window
